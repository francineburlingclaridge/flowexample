package com.example.application.views.about;

import com.example.application.services.PersonViewModel;
import com.example.application.views.main.MainView;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.beans.factory.annotation.Autowired;

@Route( value = "about", layout = MainView.class )
@RouteAlias(value = "", layout = MainView.class)
@PageTitle( "About" )
public class AboutView extends FlexLayout implements HasComponents
{
    
    private static final long            serialVersionUID = -8080431104319206448L;
    private final        PersonViewModel personViewModel;
    
    // This is the Java companion file of a design
    // You can find the design file inside /frontend/views/
    public AboutView( @Autowired final PersonViewModel personViewModel )
    {
        AboutWithEndpoint endpoint = new AboutWithEndpoint();
        AboutWithClientCallable callable = new AboutWithClientCallable( personViewModel );
        
        AboutWithId withIdLeft = new AboutWithId();
        AboutWithId withIdRight = new AboutWithId();
        
        this.add( new VerticalLayout( new HorizontalLayout( endpoint, callable ), new HorizontalLayout( withIdLeft, withIdRight ) ) );
        
        this.personViewModel = personViewModel;
        // imagine this is from a parameter passed in from e.g. HasUrlParameter
        this.personViewModel.setMyPersonId( 1 );
        this.personViewModel.addListener( person -> {
            endpoint.updateTemplate();
            callable.updateTemplate();
    
            withIdLeft.updateTemplate( person );
            withIdRight.updateTemplate( person );
        } );
    
        withIdLeft.init( personViewModel );
        withIdRight.init( personViewModel );
    }
}
