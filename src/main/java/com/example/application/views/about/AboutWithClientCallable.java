package com.example.application.views.about;

import com.example.application.services.PersonViewModel;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;

@Tag( "about-with-clientcallable" )
@JsModule( "./views/about/about-with-clientcallable.ts" )
public class AboutWithClientCallable extends LitTemplate
{
    private static final long            serialVersionUID = 8431253897155929766L;
    private final        PersonViewModel personViewModel;
    
    public AboutWithClientCallable( PersonViewModel personViewModel )
    {
        this.personViewModel = personViewModel;
    }
    
    public void updateTemplate()
    {
        getUI().ifPresent( ui -> ui.access( () -> {
            this.getElement().callJsFunction( "updateTemplate" );
        } ) );
    }
    
    @ClientCallable
    public void setPersonName( String name )
    {
        this.personViewModel.setPersonName_NotifyWithBasicListeners( name );
    }
}
