package com.example.application.views.about;

import com.example.application.services.Person;
import com.example.application.services.PersonViewModel;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.template.Id;
import com.vaadin.flow.component.textfield.TextField;

@Tag( "about-with-id" )
@JsModule( "./views/about/about-with-id.ts" )
public class AboutWithId extends LitTemplate
{
    private static final long            serialVersionUID = -6529248749200499798L;
    private              PersonViewModel personViewModel;
    
    @Id( "name" )
    private TextField name;
    
    @Id( "changeNameButton" )
    private Button changeNameButton;
    
    public AboutWithId()
    {
    }
    
    public void init( PersonViewModel personViewModel )
    {
        this.personViewModel = personViewModel;
        this.changeNameButton.addClickListener( this::setPersonName );
        
        // this should instead be done with a binder, but this is a super simple example
        this.name.setValue( this.personViewModel.getMyPerson().getName() );
    }
    
    private void setPersonName( ClickEvent<Button> buttonClickEvent )
    {
        this.personViewModel.setPersonName_NotifyWithBasicListeners( this.name.getValue() );
    }
    
    public void updateTemplate( Person person )
    {
        // this should instead be done with a binder, but this is a super simple example
        // When called from @EndPoint error -> Caused by: java.lang.IllegalStateException: Cannot access state in VaadinSession or UI without locking the session.
        this.name.setValue( person.getName() );
    
        getUI().ifPresent( ui -> ui.access( () -> {
            // these don't fire when triggered from @EndPoint
            this.name.setValue( person.getName() );
        } ) );
    }
}
