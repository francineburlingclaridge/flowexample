package com.example.application.views.about;

import com.example.application.services.Person;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;

@Tag( "about-with-endpoint" )
@JsModule( "./views/about/about-with-endpoint.ts" )
public class AboutWithEndpoint extends LitTemplate
{
    private static final long serialVersionUID = -6529248749200499798L;
    
    public AboutWithEndpoint()
    {
    }
    
    public void updateTemplate()
    {
        getUI().ifPresent( ui -> ui.access( () -> {
            // these don't fire when triggered from @EndPoint
            this.getElement().callJsFunction( "updateTemplate" );
            // this.getElement().setPropertyBean( "person", person );
        } ) );
    }
}
