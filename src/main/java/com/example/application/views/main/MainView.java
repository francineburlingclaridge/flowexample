package com.example.application.views.main;

import com.example.application.views.about.AboutView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;

import java.util.Optional;

/**
 * The main view is a top-level placeholder for other views.
 */
public class MainView extends AppLayout
{
    
    private static final long serialVersionUID = -6845702965780995053L;
    private final        Tabs menu;
    private              H1   viewTitle;
    
    public MainView()
    {
        this.setPrimarySection( Section.DRAWER );
        this.addToNavbar( true, this.createHeaderContent() );
        this.menu = this.createMenu();
        this.addToDrawer( this.createDrawerContent( this.menu ) );
    }
    
    private Component createHeaderContent()
    {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.setId( "header" );
        layout.getThemeList().set( "dark", true );
        layout.setWidthFull();
        layout.setSpacing( false );
        layout.setAlignItems( FlexComponent.Alignment.CENTER );
        layout.add( new DrawerToggle() );
        this.viewTitle = new H1();
        layout.add( this.viewTitle );
        layout.add( new Avatar() );
        return layout;
    }
    
    private Component createDrawerContent( final Tabs menu )
    {
        final VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setPadding( false );
        layout.setSpacing( false );
        layout.getThemeList().set( "spacing-s", true );
        layout.setAlignItems( FlexComponent.Alignment.STRETCH );
        final HorizontalLayout logoLayout = new HorizontalLayout();
        logoLayout.setId( "logo" );
        logoLayout.setAlignItems( FlexComponent.Alignment.CENTER );
        logoLayout.add( new Image( "images/logo.png", "My App logo" ) );
        logoLayout.add( new H1( "My App" ) );
        layout.add( logoLayout, menu );
        return layout;
    }
    
    private Tabs createMenu()
    {
        final Tabs tabs = new Tabs();
        tabs.setOrientation( Tabs.Orientation.VERTICAL );
        tabs.addThemeVariants( TabsVariant.LUMO_MINIMAL );
        tabs.setId( "tabs" );
        tabs.add( this.createMenuItems() );
        return tabs;
    }
    
    private Component[] createMenuItems()
    {
        return new Tab[] { createTab( "About", AboutView.class ) };
    }
    
    private static Tab createTab( final String text, final Class<? extends Component> navigationTarget )
    {
        final Tab tab = new Tab();
        tab.add( new RouterLink( text, navigationTarget ) );
        ComponentUtil.setData( tab, Class.class, navigationTarget );
        return tab;
    }
    
    @Override
    protected void afterNavigation()
    {
        super.afterNavigation();
        this.getTabForComponent( this.getContent() ).ifPresent( this.menu::setSelectedTab );
        this.viewTitle.setText( this.getCurrentPageTitle() );
    }
    
    private Optional<Tab> getTabForComponent( final Component component )
    {
        return this.menu.getChildren().filter( tab -> ComponentUtil.getData( tab, Class.class ).equals( component.getClass() ) )
                .findFirst().map( Tab.class::cast );
    }
    
    private String getCurrentPageTitle()
    {
        final PageTitle title = this.getContent().getClass().getAnnotation( PageTitle.class );
        return title == null ? "" : title.value();
    }
}
