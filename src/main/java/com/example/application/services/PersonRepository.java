package com.example.application.services;

import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository
{
    public Person fetchPerson( int id )
    {
        Person person = new Person();
        person.setId( id );
        person.setName( "Test" );
        return person;
    }
}
