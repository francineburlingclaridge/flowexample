package com.example.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
@SessionScope
public class PersonViewModel
{
    private final PersonRepository personRepository;
    private       Person           myPerson;
    private       Integer          myPersonId;
    
    private final Set<Listener<Person>> listeners = ConcurrentHashMap.newKeySet();
    
    public PersonViewModel( @Autowired final PersonRepository personRepository )
    {
        this.personRepository = personRepository;
    }
    
    public void setMyPersonId( final Integer myPersonId )
    {
        this.myPersonId = myPersonId;
    }
    
    public Person getMyPerson()
    {
        if ( this.myPerson == null )
        {
            this.myPerson = this.personRepository.fetchPerson( this.myPersonId );
        }
        return this.myPerson;
    }
    
    public void setPersonName_NotifyWithBasicListeners( final String name )
    {
        this.myPerson.setName( name );
        this.listeners.forEach( listener -> listener.onChanged( this.myPerson ) );
    }
    
    public void addListener( Listener<Person> listener )
    {
        this.listeners.add( listener );
    }
}
