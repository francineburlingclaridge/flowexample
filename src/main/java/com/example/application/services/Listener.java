package com.example.application.services;

public interface Listener<VALUE>
{
    public void onChanged( VALUE value );
}