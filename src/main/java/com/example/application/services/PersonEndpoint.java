package com.example.application.services;

import com.example.application.services.Person;
import com.example.application.services.PersonViewModel;
import com.vaadin.flow.server.connect.Endpoint;
import com.vaadin.flow.server.connect.auth.AnonymousAllowed;
import org.springframework.beans.factory.annotation.Autowired;

@Endpoint
@AnonymousAllowed
public class PersonEndpoint
{
    private PersonViewModel personViewModel;
    
    public PersonEndpoint( @Autowired PersonViewModel personViewModel )
    {
        this.personViewModel = personViewModel;
    }
    
    public Person getPerson()
    {
        return personViewModel.getMyPerson();
    }
    
    public void setPersonName( String name )
    {
        this.personViewModel.setPersonName_NotifyWithBasicListeners( name );
    }
    
}
