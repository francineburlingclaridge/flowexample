import {LitElement} from 'lit-element';

export abstract class UpdatableLitElement extends LitElement {
    abstract updateTemplate(): void;
}