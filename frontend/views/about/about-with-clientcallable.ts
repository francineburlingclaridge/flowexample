import {customElement, html, LitElement, property} from 'lit-element';
import {getPerson} from 'Frontend/generated/PersonEndpoint';
import Person from 'Frontend/generated/com/example/application/services/Person';
import PersonModel from 'Frontend/generated/com/example/application/services/PersonModel';
import {UpdatableLitElement} from 'Frontend/updatable-lit-element';

@customElement('about-with-clientcallable')
export class AboutWithClientcallable extends UpdatableLitElement {
    @property() person: Person = PersonModel.createEmptyValue();

    // Do not use a shadow root
    createRenderRoot() {
        this.updateTemplate();
        return this;
    }

    render() {
        return html`
            <style>
                .root {
                    margin: 20px;
                }
            </style>
            <div class="root">
                <div>Using @Enpoint to fetch data - updating with client callable</div>
                <vaadin-text-field id="name" @change=${(e:any) => this.person.name = e.target.value} label="Person name:" value="${this.person.name}"></vaadin-text-field>
                <div>${this.startsWithT()}</div>
                <vaadin-button @click=${this.setName}>Change name using @ClientCallable</vaadin-button>
            </div>`;
    }

    async setName() {
        // doesn't recognise $server for some reason
        // @ts-ignore
        await this.$server.setPersonName(this.person.name);
    }

    updateTemplate(): void {
        getPerson()
            .then(person => {
                this.person = person;
            })
            .catch(console.error);
    }

    startsWithT(): String {
        if (this.person?.name?.startsWith('T') ) {
            return 'Name starts with T';
        } else {
            return 'Name starts with something else';
        }
    }
}