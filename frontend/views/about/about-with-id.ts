import {customElement, html, LitElement, property} from 'lit-element';
import PersonModel from 'Frontend/generated/com/example/application/services/PersonModel';
import Person from 'Frontend/generated/com/example/application/services/Person';
import {getPerson, setPersonName} from 'Frontend/generated/PersonEndpoint';
import {UpdatableLitElement} from 'Frontend/updatable-lit-element';

@customElement('about-with-id')
export class AboutWithId extends LitElement {
    render() {
        return html`
            <style>
                .root {
                    margin: 20px;
                }
            </style>
            <div class="root">
                <div>Implemented correctly with @Id annotations</div>
                <vaadin-text-field id="name" label="Person name:"></vaadin-text-field>
                <div>${this.startsWithT()}</div>
                <vaadin-button id="changeNameButton">Change name using java click listener</vaadin-button>
            </div>`;
    }

    startsWithT(): String {
        return 'How do I get information about the model here?'
    }
}