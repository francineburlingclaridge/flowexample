import {customElement, html, LitElement, property} from 'lit-element';
import PersonModel from 'Frontend/generated/com/example/application/services/PersonModel';
import Person from 'Frontend/generated/com/example/application/services/Person';
import {getPerson, setPersonName} from 'Frontend/generated/PersonEndpoint';
import {UpdatableLitElement} from 'Frontend/updatable-lit-element';

@customElement('about-with-endpoint')
export class AboutWithEndpoint extends UpdatableLitElement {
    @property({reflect: true, attribute: true}) person: Person = PersonModel.createEmptyValue();

    // Do not use a shadow root
    createRenderRoot() {
        this.updateTemplate();
        return this;
    }

    render() {
        return html`
            <style>
                .root {
                    margin: 20px;
                }
            </style>
            <div class="root">
                <div>Using @Enpoint to fetch data</div>
                <vaadin-text-field id="name" @change=${(e:any) => this.person.name = e.target.value} label="Person name:" value="${this.person.name}"></vaadin-text-field>
                <div>${this.startsWithT()}</div>
                <vaadin-button @click=${this.setName}>Change name using @EndPoint</vaadin-button>
            </div>`;
    }

    setName() {
        setPersonName(this.person.name);
    }

    updateTemplate(): void {
        getPerson()
            .then(person => {
                this.person = person;
            })
            .catch(console.error);
    }

    startsWithT(): String {
        if (this.person?.name?.startsWith('T') ) {
            return 'Name starts with T';
        } else {
            return 'Name starts with something else';
        }
    }
}